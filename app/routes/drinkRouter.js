const express = require("express");

const {
    getAllDrinksMiddleware,
    getDrinkMiddleware,
    postAllDrinksMiddleware,
    putAllDrinksMiddleware,
    deleteAllDrinksMiddleware
} = require("../middleware/drink-middleware");

const drinkRouter = express.Router();

drinkRouter.get("/drinks", getAllDrinksMiddleware , (req,res) =>{
    res.json({
        message: "Get All Drinks"
    })
})

drinkRouter.get("/drinks/:drinkId", getDrinkMiddleware , (req,res) =>{
    let drinkId = req.params.drinkId;
    res.json({
        message: `Get a drink with Id: ${drinkId}`
    })
})

drinkRouter.post("/drinks", postAllDrinksMiddleware , (req,res) =>{
    res.json({
        message: "Create All Drinks"
    })
})

drinkRouter.put("/drinks/:drinkId", putAllDrinksMiddleware , (req,res) =>{
    let drinkId = req.params.drinkId;
    res.json({
        message: `Update a drink with id: ${drinkId}`
    })
})

drinkRouter.delete("/drinks/:drinkId", deleteAllDrinksMiddleware , (req,res) =>{
    let drinkId = req.params.drinkId;
    res.json({
        message: `Delete a drink with id: ${drinkId}`
    })
})

module.exports = {drinkRouter};