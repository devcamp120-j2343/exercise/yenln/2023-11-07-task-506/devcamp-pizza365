const getAllDrinksMiddleware = (req, res, next)=>{
    console.log("Get All Drink!");

    next();
}

const getDrinkMiddleware = (req, res, next)=>{
    console.log("Get a Drink!");

    next();
}

const postAllDrinksMiddleware = (req, res, next)=>{
    console.log("Create a New Drink!");
    next();
}

const putAllDrinksMiddleware = (req, res, next)=>{
    console.log("Update a Drink!");
    next();
}

const deleteAllDrinksMiddleware = (req, res, next)=>{
    console.log("Delete a Drink!");
    next();
}

module.exports = {
    getAllDrinksMiddleware,
    getDrinkMiddleware,
    postAllDrinksMiddleware,
    putAllDrinksMiddleware,
    deleteAllDrinksMiddleware
}