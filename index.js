//khai báo thư viện express
const express = require("express");
const {drinkRouter} = require("./app/routes/drinkRouter");

//khai báo app
const app = express();

//khai báo port 
const port = 8000;

app.use("/", drinkRouter);

app.listen(port, () => {
    console.log(`App listen on port ${port}`);
})